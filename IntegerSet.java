class IntegerSet {
    private final static int MAXSETVALUE = 1000;
    private boolean data[] = new boolean[MAXSETVALUE + 1];

    /**
     * Constructor for the IntegerSet
     *
     * @param setOfInt var args of int that is between 0 and the MAXSETVALUE and takes in any number of param
     * @author Joseph Chung
     * @version 1.0
     */

    public IntegerSet(int...setOfInt) {
        for (int i = 0; i < setOfInt.length; i++) {
            if (setOfInt[i] >= 0 && setOfInt[i] <= MAXSETVALUE) {
                data[setOfInt[i]] = true;
             }
        }
    }

    /**
     * Inserts the element into the IntegerSet
     *
     * @param element the element to be inserted into the IntegerSet
     * @author Joseph Chung
     * @version 1.0
     */

    public void insertElement(int element) {
        if (element >= 0 && element <= MAXSETVALUE) {
            data[element] = true;
        }
    }

    /**
     * Deletes the element from the IntegerSet
     *
     * @param element the element to be deleted from the IntegerSet
     * @author Joseph Chung
     * @version 1.0
     */

    public void deleteElement(int element) {
        if (element >= 0 && element <= MAXSETVALUE) {
            data[element] = false;
        }
    }

    /**
     * The tostring for the IntegerSet
     *
     * @return the string format of the IntegerSet
     * @author Joseph Chung
     * @version 1.0
     */

    public String toString() {
        StringBuilder str = new StringBuilder();
        int size = 0;
        int first = 0;
        str.append("{");
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i] == true) {
                size++;
                first = i;
                str.append(i).append(", ");
            }
        }
        if (size == 0) {
            return "{}";

        } else if (size == 1) {
            return "{" + first + "}";
        } else {
            str.deleteCharAt(str.length() - 1);
            str.deleteCharAt(str.length() - 1);
            return str.append("}").toString();
        }
    }

    /**
     * Compares two IntegerSets and checks if they're equal
     *
     * @param set the IntegerSet that is being compared to
     * @return returns a bool value to show if the sets are equal or not
     * @author Joseph Chung
     * @version 1.0
     */

    public boolean equals(IntegerSet set) {
        for(int i = 0; i < set.data.length; i++) {
            if (this.data[i] != set.data[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the element exists within the IntegerSet
     *
     * @param element the int that is being checked if it exists
     * @return returns a bool to show if the element exists
     * @author Joseph Chung
     * @version 1.0
     */

    public boolean hasElement(int element) {
        if (element >= 0 && element <= MAXSETVALUE) {
            if (this.data[element] == true) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates the intersection set of the sets int the parameter
     *
     * @param s1 the first IntegerSet object
     * @param s2 the second IntegerSet object
     * @author Joseph Chung
     * @version 1.0
     */

    public void intersectionOf(IntegerSet s1, IntegerSet s2) {
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = false;
        }
        for (int i = 0; i < this.data.length; i++) {
            if (s1.data[i] == true && s2.data[i] == true) {
                this.data[i] = true;
            }
        }
    }

    /**
     * Creates the union set of the sets int the parameter
     *
     * @param s1 the first IntegerSet object
     * @param s2 the second IntegerSet object
     * @author Joseph Chung
     * @version 1.0
     */

    public void unionOf(IntegerSet s1, IntegerSet s2) {
        for (int i = 0; i <this.data.length; i++) {
            this.data[i] = false;
        }
        for (int i = 0; i < this.data.length; i++) {
            if (s1.data[i] == true || s2.data[i] == true) {
                this.data[i] = true;
            }
        }
    }

    /**
     * Returns the max set value
     *
     * @return returns int the max set value
     * @author Joseph Chung
     * @version 1.0
     */

    public static int getMaxSetValue(){ return MAXSETVALUE; }

}
